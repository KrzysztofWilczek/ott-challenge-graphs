﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataManagement;

namespace WebGraph.Controllers
{
    /// <summary>
    /// Graph controller working as REST endpoint for AJAX request
    /// </summary>
    public class GraphController : System.Web.Http.ApiController
    {
        INodeDAO nodeDAO;

        public GraphController()
        {
            nodeDAO = new NodeDAO();
        }

        [HttpGet]
        public List<NodeDTO> Fetch()
        {
            return nodeDAO.fetchAll();
        }

        [HttpGet]
        public List<PathDTO> Find(string startNodeId, string targetNodeId)
        {
            return nodeDAO.findPath(startNodeId, targetNodeId);
        }
    }
}

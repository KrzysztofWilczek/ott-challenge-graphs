﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebGraph.Controllers
{
    /// <summary>
    /// Home controller for some starting page.
    /// It can be separated to html 5 standalone app.
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
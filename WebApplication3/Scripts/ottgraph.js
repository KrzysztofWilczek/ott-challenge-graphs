﻿var nodesDiagram;

function bindPathFindSubmit() {

    var url = "api/graph/find";
    $("#findPathForm").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: url,
            data: $(this).serialize(),
            success: function (data) {
                $.each(data, function (key, item) {
                    changeNodeColor(item.from);
                    changeNodeColor(item.to);
                });
                
            }
        });
    });
}

function changeNodeColor(nodeKey) {
    var node = nodesDiagram.findNodeForKey(nodeKey);
    var shape = node.findObject("SHAPE");
    shape.fill = "red";
}

function resetNodesColorToBasic() {
    nodesDiagram.startTransaction("change color");
    var it = nodesDiagram.nodes;
    while (it.next()) {
        var node = it.value;
        var shape = node.findObject("SHAPE");
        if (shape !== null) {
            shape.fill = "orange";
        }
    }
    nodesDiagram.commitTransaction("change color");
}

function loadGraphData() {

    var $go = go.GraphObject.make;
    nodesDiagram = $go(go.Diagram, "graphView", {
        initialContentAlignment: go.Spot.Center
    });

    nodesDiagram.nodeTemplate =
        $go(go.Node, "Auto",
        $go(go.Shape, "RoundedRectangle", { strokeWidth: 0, name: 'SHAPE' },
        new go.Binding("fill", "color")),
        $go(go.TextBlock,
        { margin: 8 },
        new go.Binding("text", "key"))
    );

    var url = 'api/graph/fetch';

    $.getJSON(url).done(function (data) {
        var nodes = [];
        var relations = [];
        $.each(data, function (key, item) {
            var node = { key: item.id, text: item.label, color: "orange" };
            nodes.push(node);
            $.each(item.adjacentNodes, function (targetKey, targetNode) {
                var relation = { from: item.id, to: targetNode };
                relations.push(relation);
            });
        });
        nodesDiagram.model = new go.GraphLinksModel(nodes, relations);
    });
}
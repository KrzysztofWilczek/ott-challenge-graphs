﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neo4jClient;

namespace DataManagement
{
    /// <summary>
    /// Nodes relationship representation
    /// </summary>
    public class NodesRelationship : Relationship, IRelationshipAllowingSourceNode<NodeEntity>, IRelationshipAllowingTargetNode<NodeEntity>
    {
        public static readonly string TypeKey = "NODES_RELATION";

        public NodesRelationship(NodeReference targetNode)
            : base(targetNode)
        { }

        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}

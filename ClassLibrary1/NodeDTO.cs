﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement
{
    /// <summary>
    /// Data Transfer Object for nodes
    /// </summary>
    public class NodeDTO
    {
        public string id { get; set; }
        public string label { get; set; }
        public List<string> adjacentNodes = new List<string>();
    }

    /// <summary>
    /// Builder for NodeDTO objects
    /// </summary>
    public class NodeDTOBuilder
    {
        private NodeDTO _nodeDTO;

        public NodeDTOBuilder()
        {
            _nodeDTO = new NodeDTO();
        }

        public NodeDTOBuilder id(string id)
        {
            _nodeDTO.id = id;
            return this;
        }

        public NodeDTOBuilder label(string label)
        {
            _nodeDTO.label = label;
            return this;
        }

        public NodeDTOBuilder adjacentNodes(List<string> adjacentNodes)
        {
            _nodeDTO.adjacentNodes = new List<string>(adjacentNodes);
            return this;
        }

        public NodeDTO build()
        {
            return _nodeDTO;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement
{
    /// <summary>
    /// Node db entity representation 
    /// </summary>
    public class NodeEntity
    {
        public string id { get; set; }   
        public string label { get; set; }
        public string[] adjacentNodes { get; set; }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement
{
    /// <summary>
    /// Data Transfer Object for graph path
    /// </summary>
    public class PathDTO
    {
        public string from { get; set; }
        public string to { get; set; }
    }

    /// <summary>
    /// The PathDTO builder
    /// </summary>
    public class PathDTOBuilder
    {
        private PathDTO _pathDTO;

        public PathDTOBuilder()
        {
            _pathDTO = new PathDTO();
        }

        public PathDTOBuilder to(string to)
        {
            _pathDTO.to = to;
            return this;
        }

        public PathDTOBuilder from(string from)
        {
            _pathDTO.from = from;
            return this;
        }

        public PathDTO build()
        {
            return _pathDTO;
        }
    }
}

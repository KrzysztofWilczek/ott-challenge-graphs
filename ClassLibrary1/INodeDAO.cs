﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement
{
    /// <summary>
    /// Data Access Object interfaces for Nodes manipulation
    /// </summary>
    public interface INodeDAO
    {
        /// <summary>
        /// Drop all data in storage
        /// </summary>
        void deleteAll();

        /// <summary>
        /// Returns all nodes definitions from DB (with relationships)
        /// </summary>
        /// <returns></returns>
        List<NodeDTO> fetchAll();
        
        /// <summary>
        /// Adds node or updates, if it already exists
        /// </summary>
        /// <param name="nodeDTO"></param>
        void addNode(NodeDTO nodeDTO);

        /// <summary>
        /// Bind two nodes with releationship in graph
        /// </summary>
        /// <param name="startNode"></param>
        /// <param name="targetNodeId"></param>
        void createReleationship(string startNode, string targetNodeId);

        /// <summary>
        /// Finds shortes path in graph from start to target node
        /// </summary>
        /// <param name="startNodeId"></param>
        /// <param name="targetNodeId"></param>
        /// <returns></returns>
        List<PathDTO> findPath(string startNodeId, string targetNodeId);
        
    }
}

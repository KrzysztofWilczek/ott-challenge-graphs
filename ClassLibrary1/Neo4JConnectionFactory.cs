﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neo4jClient;

namespace DataManagement
{
    /// <summary>
    /// Factory for Noe4J db connection clients, in this case just single connection
    /// </summary>
    public sealed class Neo4JConnectionFactory
    {
        private static readonly string _dbConnectionUri = "http://localhost:7474/db/data";
        private static readonly string _dbLogin = "neo4j";
        private static readonly string _dbPass = "admin";

        private GraphClient _client;
        private static readonly Neo4JConnectionFactory _instance = new Neo4JConnectionFactory();

        static Neo4JConnectionFactory()
        {

        }
        private Neo4JConnectionFactory()
        {
            _client = new GraphClient(new Uri(_dbConnectionUri), _dbLogin, _dbPass);
            _client.Connect();
            createIndexes();
        }

        /// <summary>
        /// Return NeoJClient db connection
        /// </summary>
        /// <returns>GraphClient</returns>
        public GraphClient getConnection()
        {
            return _client;
        }

        /// <summary>
        /// Defines indexes if not set yet
        /// </summary>
        private void createIndexes()
        {
            _client.CreateIndex("Node", new IndexConfiguration() { Provider = IndexProvider.lucene, Type = IndexType.exact }, IndexFor.Node); 
        }
        
        /// <summary>
        /// Returns Neo4JConnectionFactory instance
        /// </summary>
        public static Neo4JConnectionFactory Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}

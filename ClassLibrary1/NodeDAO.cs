﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace DataManagement
{
    /// <summary>
    /// Implementation of DAO for graph nodes using Neo4J DB client
    /// </summary>
    public class NodeDAO : INodeDAO
    {
        private GraphClient _client;

        /// <summary>
        /// Constructor gets Neo4J connection client
        /// </summary>
        public NodeDAO()
        {
            _client = Neo4JConnectionFactory.Instance.getConnection();
        }

        public void addNode(NodeDTO nodeDTO)
        {
            NodeEntity entity = new NodeEntity() {
                id = nodeDTO.id,
                label = nodeDTO.label,
                adjacentNodes = nodeDTO.adjacentNodes.ToArray()
            };

            Node<NodeEntity> nodeEntity = getNodeById(entity.id);
            if (nodeEntity == null)
            {
                _client.Cypher
                    .Create("(node:Node {entity})")
                    .WithParam("entity", entity)
                    .ExecuteWithoutResults();//.Create(entity);
            } else
            {
                _client.Update(
                    nodeEntity.Reference,
                    node =>
                    {
                        node.label = entity.label;
                        node.adjacentNodes = entity.adjacentNodes;
                    });
            }
            if (nodeDTO.adjacentNodes.Any())
            {
                createRelationshipsFromList(nodeDTO.id, nodeDTO.adjacentNodes);
            }
        }

        public void createRelationshipsFromList(string startNodeId, List<string> targetNodeIds)
        {
            foreach (string targetNodeId in targetNodeIds)
            {
                createReleationship(startNodeId, targetNodeId);       
            }
        }

        /// <summary>
        /// Get node with reference by node ID
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns>Node with reference</returns>
        private Node<NodeEntity> getNodeById(string nodeId)
        {
            var result = _client.Cypher
                .Match("(node:Node)")
                .Where((NodeEntity node) => node.id == nodeId)
                .Return<Node<NodeEntity>>("node")
                .Results;
            if (result.Any())
            {
                return result.Single();
            } else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates temporary node in case we have relationship data before node definition 
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        private Node<NodeEntity> createTemporaryNode(string nodeId) {
            NodeDTO nodeDTO = new NodeDTOBuilder().id(nodeId).build();
            addNode(nodeDTO);
            return getNodeById(nodeId);
        }

        public void createReleationship(string startNodeId, string targetNodeId)
        {
            Node<NodeEntity> startNode = getNodeById(startNodeId);
            Node<NodeEntity> targetNode = getNodeById(targetNodeId);
            if (startNode != null)
            {
                if (targetNode == null)
                {
                    targetNode = createTemporaryNode(targetNodeId);
                }
                _client.CreateRelationship(startNode.Reference, new NodesRelationship(targetNode.Reference));
            }
        }

        public void deleteAll()
        {
            _client.Cypher
                .OptionalMatch("(node:Node)<-[r]-()")
                .OptionalMatch("(node:Node)-[k]->()")
                .Delete("node, r, k")
                .ExecuteWithoutResults();

            _client.Cypher
                .Match("(node:Node)")
                .Delete("node")
                .ExecuteWithoutResults();
        }

        public List<NodeDTO> fetchAll()
        {
            List<NodeDTO> listOfNodes = new List<NodeDTO>();
            var result = _client.Cypher
              .Match("(node:Node)")
              .Return<NodeEntity>("node")
              .Results;
            foreach (NodeEntity entity in result) {
                listOfNodes.Add(new NodeDTOBuilder().id(entity.id).label(entity.label).adjacentNodes(entity.adjacentNodes.OfType<string>().ToList()).build());
            }
            return listOfNodes;
        }

        public List<PathDTO> findPath(string startNodeId, string targetNodeId)
        {
            List<PathDTO> pathBetweenNodes = new List<PathDTO>();

            var pathsQuery =
            _client.Cypher
                .Match("p = shortestPath((start:Node)-[*..150]->(end:Node))")
                .Where((NodeEntity start) => start.id == startNodeId)
                .AndWhere((NodeEntity end) => end.id == targetNodeId)
                .Return(p => new PathsResult<NodeEntity>
                {
                    Nodes = Return.As<IEnumerable<Node<NodeEntity>>>("nodes(p)"),
                    Relationships = Return.As<IEnumerable<RelationshipInstance<object>>>("rels(p)")
                });
            var res = pathsQuery.Results;
            if (res.Any())
            {
                PathsResult<NodeEntity> result = res.First();          
                foreach (RelationshipInstance<object> relation in result.Relationships)
                {
                    PathDTO pathDTO = new PathDTOBuilder()
                        .from(result.Nodes.First(node => node.Reference == relation.StartNodeReference).Data.id)
                        .to(result.Nodes.First(node => node.Reference == relation.EndNodeReference).Data.id)
                        .build();
                    pathBetweenNodes.Add(pathDTO);
                }
            }
            return pathBetweenNodes;
        }
    }

    /// <summary>
    /// Helper class for handling search queries results from Neo4J
    /// </summary>
    /// <typeparam name="TNode"></typeparam>
    public class PathsResult<TNode>
    {
        public IEnumerable<Node<TNode>> Nodes { get; set; }
        public IEnumerable<RelationshipInstance<object>> Relationships { get; set; }
    }
}

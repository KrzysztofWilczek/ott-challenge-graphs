# README #

Some basic OTT Graphs challange implementation - using Neo4J 

### Instalation ###

To sort out graph path searching problem we can use Neo4J graph database. This DB is used in the solution, so to be able to run it, you have to first install Neo4J DB.

1. Go to http://neo4j.com/download/ and download version of database for your platform.
2. Follow instructions in Neo4J installer.
3. Run Neo4J 
4. Now you can open database model files in any catalog you like. By default Neo4J will start from http://localhost:7474/browser/
5. You can use default neo4J credentials to access database. But You will have to change password - set user neo4j and pass to admin. (requried by app settings)

### Implementation notes ###

Solution contains 3 different parts:

* DataLoader - which is console app 
* DataManagement - which is really a repository, which gives DAO layer for Neo4J
* WebGraph - which is ASP web app (with little REST part)

Both DataLoader and WebGraph needs Neo4J DB working on localhost:7474 

### DataLoader ###

DataLoader takes directory paths from run arguments. Buy default it is current app catalog, but You can provide a list of catalogs (just as separated by space char).
DataLoader then

* takes all *.xml files.
* drop old data in Neo4J
* import node by node with their relationships
* each import ends with console log (it should be really some log)

### DataManagement ###

In NodeDAO You can find hardcoded uri, login and pass for database. Those should be moved to some app params. 
DAO has got interface and there is also DTO part which is used to create transparency for Neo4J DAO implementation. 

### WebGraph ###

Web Graph is ASP + little REST API. In general front-end interface is just HTML + JS based. It is using GOJS for showing graphs. It can be full standalone app. But now we have just single controller with very simple view.
GraphController is used to communicate user requests with DAO layer. It respond just to GET requests and gives only JSON. 

Sample request for fetching full graph

```
http://localhost:64598/api/graph/fetch
```

Sample request for finding path between two nodes
```
http://localhost:64598/api/graph/find?startNodeId=2&targetNodeId=1
```

Interface for finding nodes is simple also:

* if path exists then it gives list of pairs (from - to)
* if not exists then it just doesn't pass pairs

### Summary ###

Current implementation is based on some missing requriements:

* I didn't know how big nodes database we can expect (now to speed up nodes loading there is some data duplication for relations, which we may like to avoid)
* How long we can waiting before we find path (there is 150 ms searching limit now)
* How big graph we like to show at once on web pages (how parts loading should look like)
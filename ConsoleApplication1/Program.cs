﻿using System;
using System.Collections.Generic;
using System.IO;
using DataManagement;

namespace DataLoader
{
    class Program
    {
        /// <summary>
        /// Checking console app arguments to set right xml files directory paths, 
        /// by default it will set current application base path
        /// </summary>
        /// <param name="args">console app arguments</param>
        /// <returns>list of searchable ingest paths</returns>
        private static string[] prepareDirectoryPath(string[] args) 
        {
            if (args == null || args.Length == 0)
            {
                args = new string[] { AppDomain.CurrentDomain.BaseDirectory };
            }
            return args;
        }

        /// <summary>
        /// Gets list of xml files paths from selected catalogs
        /// </summary>
        /// <param name="catalogs">list of catalogs paths</param>
        /// <returns>list of ingest file paths</returns>
        private static List<string> getFilesFromDirectory(string[] catalogs)
        {
            List<string> filePaths = new List<string>();
            foreach (string catalog in catalogs) {
                try
                {
                    filePaths.AddRange(Directory.GetFiles(@catalog, "*.xml"));
                }
                catch (DirectoryNotFoundException)
                {
                    Console.WriteLine("Catalog does not exists");
                    Environment.Exit(0);
                }
            }
            return filePaths;
        }

        static void Main(string[] args)
        {
            args = Program.prepareDirectoryPath(args);
            List<string> xmlFilePaths = Program.getFilesFromDirectory(args);
            XMLFileReader xmlFileReader = XMLFileReader.Instance;
            NodeDAO nodeDAO = new NodeDAO();
            // drop all previous data from database
            nodeDAO.deleteAll();
            
            // xml files parsing - adds one node and its relationships into DB
            foreach (string xmlFilePath in xmlFilePaths)
            {
                NodeXML nodeXML = xmlFileReader.readFile(xmlFilePath);
                NodeDTO nodeDTO = new NodeDTOBuilder().id(nodeXML.id).label(nodeXML.label).adjacentNodes(nodeXML.adjacentNodes).build();
                nodeDAO.addNode(nodeDTO);
                Console.WriteLine("New graph node added: "+nodeXML.label);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace DataLoader
{
    /// <summary>
    /// File reader class for single Node XML file parsing
    /// </summary>
    public sealed class XMLFileReader
    {
        private XmlSerializer serializer;
        private static readonly XMLFileReader instance = new XMLFileReader();
        static XMLFileReader()
        {
        }
        private XMLFileReader()
        {
            serializer = new XmlSerializer(typeof(NodeXML), new XmlRootAttribute("node"));
        }

        /// <summary>
        /// Reads single file into NodeXML object
        /// </summary>
        /// <param name="xmlFilePath">path to the ingest file</param>
        /// <returns>NodeXML node ingest data</returns>
        public NodeXML readFile(string xmlFilePath)
        {
            NodeXML nodeXML = null;
            try
            {
                FileStream fs = new FileStream(xmlFilePath, FileMode.Open);
                XmlReader reader = XmlReader.Create(fs);
                nodeXML = (NodeXML)serializer.Deserialize(reader);
                fs.Dispose();
            }
            catch (FileNotFoundException) { throw new DataLoaderException("Cannot find XML file: "+xmlFilePath); }
            catch (IOException) { throw new DataLoaderException("Cannot read XML file: "+xmlFilePath); }
            catch (InvalidOperationException) { throw new DataLoaderException("Cannot parse XML file: " + xmlFilePath); }
            return nodeXML;
        }

        /// <summary>
        /// Return single instance of XMLFileReader
        /// </summary>
        public static XMLFileReader Instance
        {
            get
            {
                return instance;
            }
        }
    }
}

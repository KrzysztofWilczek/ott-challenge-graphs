﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataLoader
{
    /// <summary>
    /// Data mapper for node XML
    /// </summary>
    public class NodeXML
    {
        [XmlElement("id")]
        public string id { get; set; }
        [XmlElement("label")]
        public string label { get; set; }

        [XmlArray("adjacentNodes")]
        [XmlArrayItem("id")]
        public List<string> adjacentNodes = new List<string>();
    }
}

﻿using System;

/// <summary>
/// Exception specified fo XMLFileReader
/// </summary>
public class DataLoaderException : Exception
{
    public DataLoaderException()
    {
    }

    /// <summary>
    /// Just puth info into console - may be doing some custom logging
    /// </summary>
    /// <param name="message"></param>
    public DataLoaderException(string message)
        : base(message)
    {
        Console.WriteLine(message);
    }

    public DataLoaderException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
